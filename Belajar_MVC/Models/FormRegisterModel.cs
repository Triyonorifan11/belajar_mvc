﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Belajar_MVC.Models
{
    public class FormRegisterModel
    {
        [Required(ErrorMessage = "Mohon Masukkan Email")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mohon Masukkan Password")]
        [StringLength(100, ErrorMessage = "{0} Harus Lebih {2} karakter.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Mohon Masukkan Nama")]
        [Display(Name = "Nama Lengkap")]
        public string Nama { get; set; }

        [Required(ErrorMessage = "Mohon Masukkan Jenis Kelamin")]
        [Display(Name = "Jenis Kelamin")]
        public Gender Jk { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Password Tidak Sama")]
        public string ConfirmPassword { get; set; }
    }

    public enum Gender
    {
        L, P
    }

    public class LoginUserModel
    {
        [Required(ErrorMessage = "Mohon Masukkan Email Anda")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Mohon Masukkan Password")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }

    public class DataTableAjaxPostModel
    {
        public int draw { get; set; }
        public int start { get; set; }
        public int length { get; set; }
        public List<Column> columns { get; set; }
        public Search search { get; set; }
        public List<Order> order { get; set; }

    }

    public class Column
    {
        public string data { get; set; }
        public string name { get; set; }
        public string alias { get; set; }
        public bool searchable { get; set; }
        public bool orderable { get; set; }
        public Search search { get; set; }
    }

    public class Search
    {
        public string value { get; set; }
        public string regex { get; set; }
    }

    public class Order
    {
        public int column { get; set; }
        public string dir { get; set; }
    }

    public class m_tb_hobi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public m_tb_hobi()
        {
            this.tb_user_d_hobi = new HashSet<tb_user_d_hobi>();
        }

        [Required(ErrorMessage = "Mohon Masukkan Nama Hobi")]
        [Display(Name = "Nama Hobi")]
        public string nama_hobi { get; set; }
        public string create_by { get; set; }
        public System.DateTime create_at { get; set; }
        public string update_by { get; set; }
        public System.DateTime update_at { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tb_user_d_hobi> tb_user_d_hobi { get; set; }
    }

    public partial class m_tb_user_d_hobi
    {
        public int id { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> hobi_id { get; set; }
        public string create_by { get; set; }
        public Nullable<System.DateTime> create_at { get; set; }
        public string update_by { get; set; }
        public Nullable<System.DateTime> update_at { get; set; }

        public virtual tb_hobi tb_hobi { get; set; }
        public virtual tb_user tb_user { get; set; }
    }
}