﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Belajar_MVC
{
    public class GlobalClass : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the Web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            //write your handler implementation here.
        }

        #endregion
        public class ReportRequest
        {
            public string rptQuery { get; set; }
            public string rptFile { get; set; }
            /// <summary>
            /// Paper Size; 0-Default, 1-Letter, 4-Ledger, 5-Legal, 8-A3, 9-A4, 11-A5, 14-Folio
            /// </summary>
            public int rptPaperSizeEnum { get; set; }
            /// <summary>
            /// Paper Orientation: 1-Portrait, 2-Landscape
            /// </summary>
            public int rptPaperOrientationEnum { get; set; }
            public string rptExportType { get; set; }
            public Dictionary<string, string> rptParam { get; set; }
            public System.Data.DataTable rptDataSource { get; set; }
        }
    }
}
