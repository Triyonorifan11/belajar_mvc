﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Belajar_MVC.Startup))]
namespace Belajar_MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
