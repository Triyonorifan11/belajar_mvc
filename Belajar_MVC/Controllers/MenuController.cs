﻿using Belajar_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Belajar_MVC.Controllers
{
    public class MenuController : Controller
    {
        private db_belajarEntities2 db = new db_belajarEntities2();
        private string query = "";
        // GET: Menu
        public ActionResult Index()
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                return View();
            }
            return RedirectToAction("LoginUser", "Home");
        }

        public ActionResult Form(int? id)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                tb_menu tbl_menu;
                string action = "New Data";
                if (id == null)
                {
                    tbl_menu = new tb_menu();
                    tbl_menu.guid = Guid.NewGuid();
                    tbl_menu.create_by = Session["nama_user"].ToString();
                    tbl_menu.create_at = ClassFunction.GetServerTime();
                }
                else
                {
                    action = "Update Data";
                    tbl_menu = db.tb_menu.FirstOrDefault(w => w.id == id);
                }
                ViewBag.action = action;
                return View(tbl_menu);
            }
            return RedirectToAction("LoginUser", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_menu tbl, string action, string closing)
        {
            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";

            if (string.IsNullOrEmpty(tbl.name_menu))
            {
                msg += "Harap Masukkan nama menu";
            }else if(db.tb_menu.Where(d => d.name_menu == tbl.name_menu).Any())
            {
                msg += "Menu sudah ada";
            }

            if (string.IsNullOrEmpty(msg))
            {
                using(var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if(tbl.id == 0)
                        {
                            tbl.update_at = tbl.create_at;
                            tbl.update_by = tbl.create_by;
                            db.tb_menu.Add(tbl);
                        }else
                        {
                            tbl.update_by = Session["nama_user"].ToString();
                            tbl.update_at = servertime;
                            db.Entry(tbl).State = EntityState.Modified;
                        }
                        db.SaveChanges();
                        objTrans.Commit();
                        hdrid = tbl.id.ToString();
                        msg = "Data Berhasil Tersimpan, ID = " + hdrid;
                        result = "success";
                    }
                    catch(System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        msg = ClassFunction.getErrorMessage(e);
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }

            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

        // get data menu all
        public JsonResult getListDataMenu(DataTableAjaxPostModel model)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {

                var sql_select = "*";
                var sql_from = "tb_menu";
                var sFixedFilter = "";
                var sOrder_by = "id desc";
                int filteredResultsCount;
                int totalResultsCount;
                var result = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sql_select, sql_from, sFixedFilter, sOrder_by);
                return Json(
                    new
                    {
                        draw = model.draw,
                        recordsTotal = totalResultsCount,
                        recordsFiltered = filteredResultsCount,
                        data = result
                    });
            }
            return Json(new
            {
                draw = 0,
                recordsTotal = 0,
                recordsFiltered = 0,
                data = 0,
            });
        }

        //delete menu
        [HttpPost]
        public ActionResult Delete(int id, tb_menu model)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                try
                {
                    // TODO: Add delete logic here
                    var tbl_menu = db.tb_menu.Where(data => data.id == id).FirstOrDefault();
                    if (tbl_menu != null)
                    {
                        db.tb_menu.Remove(tbl_menu);
                        db.SaveChanges();
                        Session["success"] = "Berhasil Hapus Data";
                        return RedirectToAction("Index", "Menu");
                    }
                    foreach (var error in ViewData.ModelState.Values.SelectMany(v => v.Errors))
                    {
                        Session["error"] = error.ErrorMessage;
                    };
                    return RedirectToAction("Index", "Hobi");
                }
                catch (Exception err)
                {
                    Session["error"] = err;
                    return RedirectToAction("Index", "Hobi");
                }
            }
            return RedirectToAction("LoginUser", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
