﻿using Belajar_MVC.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Belajar_MVC.Controllers
{
    public class RolesController : Controller
    {
        private db_belajarEntities2 db = new db_belajarEntities2();
        private string query = "";
        // GET: Roles
        public ActionResult Index()
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                return View();
            }
            return RedirectToAction("LoginUser", "Home");
        }

           

        // GET: Roles/Create
        public ActionResult Form(int? id)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                tb_role tbl_role;
                string action = "New Data";
                if (id == null)
                {
                    tbl_role = new tb_role();
                    tbl_role.guid = Guid.NewGuid();
                    tbl_role.create_by = Session["nama_user"].ToString();
                    tbl_role.create_at = ClassFunction.GetServerTime();
                }
                else
                {
                    action = "Update Data";
                    tbl_role = db.tb_role.FirstOrDefault(w => w.id == id);
                }
                ViewBag.action = action;
                return View(tbl_role);
            }
            return RedirectToAction("LoginUser", "Home");
            

           
        }

        // POST: Roles/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Form(tb_role d_form, List<tb_role_d_menu> data_menu)
        {
            var servertime = ClassFunction.GetServerTime();
            var msg = ""; var result = "failed"; var hdrid = "";

            if (string.IsNullOrEmpty(d_form.name_role))
                msg += "Please fill Role Name field!";
            else if (db.tb_role.Where(w => w.name_role == d_form.name_role && w.guid != d_form.guid).Any())
                msg += "Role Name already used, Please fill with another name!";

            if (data_menu == null || data_menu.Count == 0)
                msg += "Please fill Detail!";

            if (string.IsNullOrEmpty(msg))
            {
                using(var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        if(d_form.id == 0)
                        {
                            d_form.update_at = d_form.create_at;
                            d_form.update_by = d_form.create_by;
                            db.tb_role.Add(d_form);
                        }
                        else
                        {
                            d_form.update_by = Session["nama_user"].ToString();
                            d_form.update_at = servertime;
                            db.Entry(d_form).State = EntityState.Modified;

                            var menu_id = data_menu.Select(s => s.id).ToList();
                            var trndtl = db.tb_role_d_menu.Where(w => w.role_id == d_form.id && !menu_id.Contains(w.id));
                            if (trndtl != null) db.tb_role_d_menu.RemoveRange(trndtl);
                        }
                        db.SaveChanges();

                        foreach(var item in data_menu)
                        {
                            var new_menu = (tb_role_d_menu)ClassFunction.MappingTable(new tb_role_d_menu(), item);
                            new_menu.role_id = d_form.id;
                            new_menu.update_at = d_form.update_at;
                            new_menu.update_by = d_form.update_by;
                            if(new_menu.id == 0)
                            {
                                new_menu.create_by = d_form.create_by;
                                new_menu.create_at = d_form.create_at;
                                db.tb_role_d_menu.Add(new_menu);
                            }else
                            {
                                db.Entry(new_menu).State = EntityState.Modified;
                            }
                        }
                        db.SaveChanges();
                        objTrans.Commit();

                        hdrid = d_form.id.ToString();
                        msg = "Data Berhasil Tersimpan, ID = " + hdrid;
                        result = "success";

                    }
                    catch (System.Data.Entity.Validation.DbEntityValidationException e)
                    {
                        objTrans.Rollback();
                        msg = ClassFunction.getErrorMessage(e);
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg, hdrid }, JsonRequestBehavior.AllowGet);
        }

       
        

        [HttpPost]
        public ActionResult GetDataAllMenu()
        {
            JsonResult r_json = null;
            try
            {
                query = $"SELECT 0 seq, id as menu_id, guid, name_menu, create_by, create_at, update_by, update_at FROM tb_menu";
                var tbl_menu = ClassFunction.toObject(new ClassConnection().GetDataTable(query, "tb_menu"));

                if (tbl_menu == null || tbl_menu.Count <= 0)
                {
                    r_json = Json(new { result = "Data Tidak Ada" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    r_json = Json(new { result = "", tbl_menu }, JsonRequestBehavior.AllowGet);
                    r_json.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception err)
            {
                r_json = Json(new { result = err.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return r_json;
        }

        public ActionResult FillDetailDataRole(int id)
        {
            JsonResult r_json = null;
            try
            {
                query = $"SELECT d.id, d.role_id, d.menu_id, format(d.create_at, 'yyyy-MM-dd HH:mm:ss') create_at, d.create_by, d.update_at, d.update_by, m.name_menu FROM tb_role_d_menu d inner join tb_menu m ON m.id = d.menu_id inner join tb_role r ON r.id = d.role_id WHERE r.id = {id} ORDER BY role_id DESC";
                var tbl_role = ClassFunction.toObject(new ClassConnection().GetDataTable(query, "tb_role"));
                if (tbl_role == null || tbl_role.Count <= 0)
                    r_json = Json(new { result = "Data tidak ada" }, JsonRequestBehavior.AllowGet);
                else
                {
                    r_json = Json(new { result = "", tbl_role }, JsonRequestBehavior.AllowGet);
                    r_json.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                r_json = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return r_json;
        }

        public JsonResult getListDataRole(DataTableAjaxPostModel model)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {

                var sql_select = "*";
                var sql_from = "tb_role";
                var sFixedFilter = "";
                var sOrder_by = "id desc";
                int filteredResultsCount;
                int totalResultsCount;
                var result = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sql_select, sql_from, sFixedFilter, sOrder_by);
                return Json(
                    new
                    {
                        draw = model.draw,
                        recordsTotal = totalResultsCount,
                        recordsFiltered = filteredResultsCount,
                        data = result
                    });
            }
            return Json(new
            {
                draw = 0,
                recordsTotal = 0,
                recordsFiltered = 0,
                data = 0,
            });
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid guid)
        {
            var msg = ""; var result = "failed";
            var tbl = db.tb_role.FirstOrDefault(w => w.guid == guid);
            if (tbl == null)
                msg = "Data can't be found!!";
            else
            {
                if (db.cekDataUsage(tbl.id, new List<string> { "role_id" }, new List<string> { "tb_role_d_menu" }))
                    msg += $"this data can't be delete, because using in another transaction!<br />";
            }

            if (msg == "")
            {
                using (var objTrans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var trndtl = db.tb_role_d_menu.Where(a => a.role_id == tbl.id);
                        db.tb_role_d_menu.RemoveRange(trndtl);

                        db.tb_role.Remove(tbl);
                        db.SaveChanges();
                        objTrans.Commit();
                        result = "success";
                    }
                    catch (Exception ex)
                    {
                        objTrans.Rollback();
                        msg = ex.ToString();
                    }
                }
            }
            return Json(new { result, msg }, JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
