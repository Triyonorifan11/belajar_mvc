﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Belajar_MVC.Models;

namespace Belajar_MVC.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        private db_belajarEntities2 db = new db_belajarEntities2();
        private string query = "";
        public ActionResult Index()
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                return View();
            }
            return RedirectToAction("LoginUser", "Home");
        }

        // GET: Dashboard/Details/5
        public ActionResult Details(int id)
        {
            return RedirectToAction("index", "Dashboard");
        }

        // GET: Dashboard/Create
        public ActionResult Create()
        {
            return RedirectToAction("index", "Dashboard");
        }

        // POST: Dashboard/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            return RedirectToAction("index", "Dashboard");
        }

        // GET: Dashboard/Edit/5
        public ActionResult Edit(int id)
        {
            return RedirectToAction("index", "Dashboard");
        }

        // POST: Dashboard/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            return RedirectToAction("index", "Dashboard");
        }

        // GET: Dashboard/Delete/5
        public ActionResult Delete(int id)
        {
            return RedirectToAction("index", "Dashboard");
        }

        // POST: Dashboard/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            return RedirectToAction("index", "Dashboard");
        }

        public ActionResult fetchTotakJK()
        {
            JsonResult r_json = null;

            try
            {
                query = "SELECT	JK, COUNT(JK) total from tb_user group by JK";
                var count_jk = ClassFunction.toObject(new ClassConnection().GetDataTable(query, "tb_user"));
                if(count_jk == null || count_jk.Count <= 0)
                {
                    r_json = Json(new { result = "Data tidak ada" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    r_json = Json(new { result = "", count_jk }, JsonRequestBehavior.AllowGet);
                    r_json.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                r_json = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }

            return r_json;
        }
    }
}
