﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Belajar_MVC.Models;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using static Belajar_MVC.GlobalClass;
using static Belajar_MVC.GlobalFunction;

namespace Belajar_MVC.Controllers
{
    public class UsersController : Controller
    {
        private db_belajarEntities2 db = new db_belajarEntities2();
        private string query = "";
        // GET: Users
        public ActionResult Index()
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                return View();
            }
            return RedirectToAction("LoginUser", "Home");
        }

        public ActionResult Profil()
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                return View();
            }
            return RedirectToAction("LoginUser", "Home");
        }

        public ActionResult GetDataProfil()
        {
            JsonResult r_json = null;
            try
            {
                query = $"SELECT Email, Nama, JK FROM tb_user WHERE Id = {Session["id_user"]}";
                var data_user = ClassFunction.toObject(new ClassConnection().GetDataTable(query, "tb_user"));
                if (data_user == null || data_user.Count <= 0)
                    r_json = Json(new { result = "Data tidak ada" }, JsonRequestBehavior.AllowGet);
                else
                {
                    r_json = Json(new { result = "", data_user }, JsonRequestBehavior.AllowGet);
                    r_json.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                r_json = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return r_json;
        }


        public JsonResult getListDataUser(DataTableAjaxPostModel model)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {

                var sql_select = "*";
                var sql_from = "tb_user";
                var sFixedFilter = "";
                var sOrder_by = "Id desc";
                int filteredResultsCount;
                int totalResultsCount;
                var result = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sql_select, sql_from, sFixedFilter, sOrder_by);
                return Json(
                    new {
                        draw = model.draw,
                        recordsTotal = totalResultsCount,
                        recordsFiltered = filteredResultsCount,
                        data = result
                    });
            }
            return Json(new {
                draw = 0,
                recordsTotal = 0,
                recordsFiltered = 0,
                data = 0,
            });
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                tb_user tbl;
                if (id == null)
                {
                    return RedirectToAction("Index", "Users");
                }
                else
                {
                    tbl = db.tb_user.FirstOrDefault(w => w.Id == id);
                    return View(tbl);
                }
            }
            return RedirectToAction("LoginUser", "Home");
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                return View();
            }
            return RedirectToAction("LoginUser", "Home");
        }

        // POST: Users/Create
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tb_user d_form, List<tb_user_d_hobi> data_hobi)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                var message = ""; var result = "failed"; var hbrid = "";
                var servertime = ClassFunction.GetServerTime();
                if (data_hobi == null || data_hobi.Count <= 0)
                {
                    message = "Mohon pilih hobi";
                    ModelState.AddModelError("Nama", message);
                }

                if (string.IsNullOrEmpty(d_form.Nama) || string.IsNullOrEmpty(d_form.JK.ToString()) || string.IsNullOrEmpty(d_form.Password)) {
                    message = "Mohon masukkan inputan dengan lengkap";
                    ModelState.AddModelError("Nama", message);
                }else if (db.tb_user.Where(data=>data.Email == d_form.Email).Any())
                {
                    message = "Email sudah ada";
                    ModelState.AddModelError("Nama", message);
                }

               


                if (string.IsNullOrEmpty(message))
                {
                    using (var obj = db.Database.BeginTransaction())
                    {
                        try
                        {
                            // TODO: Add insert logic here
                            db.tb_user.Add(d_form);
                            db.SaveChanges();

                            foreach(var item in data_hobi)
                            {
                                var new_hobi = (tb_user_d_hobi)ClassFunction.MappingTable(new tb_user_d_hobi(), item);
                                new_hobi.user_id = d_form.Id;
                                new_hobi.create_at = servertime;
                                new_hobi.update_at = servertime;
                                new_hobi.create_by = Session["nama_user"].ToString();
                                new_hobi.update_by = Session["nama_user"].ToString();
                                db.tb_user_d_hobi.Add(new_hobi);
                            }
                            db.SaveChanges();
                            obj.Commit();
                            hbrid = d_form.Id.ToString();
                            message = "Data Berhasil Dimasukkan ID " + d_form.Id;
                            Session["success"] = "Berhasil Tambah Data";
                            result = "success";

                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException err)
                        {
                            obj.Rollback();
                            message += ClassFunction.getErrorMessage(err);
                            return View(err);
                        }
                        catch(Exception err)
                        {
                            obj.Rollback();
                            message = err.ToString();
                        }
                    }

                }

                return Json(new { result, message, hbrid}, JsonRequestBehavior.AllowGet);

            }
            return RedirectToAction("LoginUser", "Home");
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int id)
        {
            return RedirectToAction("Index", "Users");
        }

        // POST: Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(tb_user d_form, List<tb_user_d_hobi> data_hobi)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                var message = ""; var result = "failed"; var hbrid = "";
                var servertime = ClassFunction.GetServerTime();
                if (data_hobi == null || data_hobi.Count <= 0)
                {
                    message = "Mohon pilih hobi";
                    ModelState.AddModelError("Nama", message);
                }

                if (string.IsNullOrEmpty(d_form.Nama) || string.IsNullOrEmpty(d_form.JK.ToString()) || string.IsNullOrEmpty(d_form.Password))
                {
                    message = "Mohon masukkan inputan dengan lengkap";
                    ModelState.AddModelError("Nama", message);
                }


                if (string.IsNullOrEmpty(message))
                {
                    using (var obj = db.Database.BeginTransaction())
                    {
                        try
                        {
                            // TODO: Add insert logic here
                            db.Entry(d_form).State = EntityState.Modified;

                            var hobi_id_ = data_hobi.Select(data => data.id).ToList();
                            var trnHobi = db.tb_user_d_hobi.Where(w => w.user_id == d_form.Id && !hobi_id_.Contains(w.id));
                            db.tb_user_d_hobi.RemoveRange(trnHobi);
                            db.SaveChanges();

                            foreach (var item in data_hobi)
                            {
                                var new_hobi = (tb_user_d_hobi)ClassFunction.MappingTable(new tb_user_d_hobi(), item);
                                new_hobi.user_id = d_form.Id;
                                new_hobi.update_at = servertime;
                                new_hobi.update_by = Session["nama_user"].ToString();
                                if(new_hobi.id == 0)
                                {
                                    new_hobi.create_at = servertime;
                                    new_hobi.create_by = Session["nama_user"].ToString();
                                    db.tb_user_d_hobi.Add(new_hobi);
                                }else
                                {
                                    db.Entry(new_hobi).State = EntityState.Modified;
                                }
                            }
                            db.SaveChanges();
                            obj.Commit();
                            hbrid = d_form.Id.ToString();
                            message = "Data Berhasil Update ID " + d_form.Id;
                            Session["success"] = "Berhasil Update Data";
                            result = "success";

                        }
                        catch (System.Data.Entity.Validation.DbEntityValidationException err)
                        {
                            obj.Rollback();
                            message += ClassFunction.getErrorMessage(err);
                            return View(err);
                        }
                        catch (Exception err)
                        {
                            obj.Rollback();
                            message = err.ToString();
                        }
                    }

                }

                return Json(new { result, message, hbrid }, JsonRequestBehavior.AllowGet);

            }
            return RedirectToAction("LoginUser", "Home");
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int id)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                return View();
            }
            return RedirectToAction("LoginUser", "Home");
        }

        // POST: Users/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(tb_user model)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {

                try
                {
                    // TODO: Add delete logic here
                    var tbl_user = db.tb_user.FirstOrDefault(w => w.Id == model.Id);
                    if(db.cekDataUsage(tbl_user.Id, new List<string> { "user_id"}, new List<string> { "tb_user_d_hobi" }))
                    {
                        Session["error"] = "Tidak dihapus, Data User memiliki transaksi lain";
                    }
                    if (tbl_user != null)
                    {
                        var trans_hobi = db.tb_user_d_hobi.Where(w => w.user_id == model.Id);
                        if (trans_hobi != null) db.tb_user_d_hobi.RemoveRange(trans_hobi);
                        db.tb_user.Remove(tbl_user);
                        db.SaveChanges();
                        Session["success"] = "Berhasil Hapus Data";
                        return RedirectToAction("Index", "Users");
                    }
                    return View(model);
                }
                catch(Exception err)
                {
                    return View("Index", err);
                }
            }
            return RedirectToAction("LoginUser", "Home");
        }

        public ActionResult FillDetailDataHobi(int id)
        {
            JsonResult r_json = null;
            try
            {
                query = $"SELECT d.id, d.user_id, d.hobi_id, format(d.create_at, 'yyyy-MM-dd HH:mm:ss') create_at, d.create_by, d.update_at, d.update_by, h.nama_hobi FROM tb_user_d_hobi d inner join tb_user u ON u.Id = d.user_id inner join tb_hobi h ON h.id = d.hobi_id WHERE u.Id = {id} ORDER BY hobi_id DESC";
                var tbl_hobi = ClassFunction.toObject(new ClassConnection().GetDataTable(query, "tb_hobi"));
                if (tbl_hobi == null || tbl_hobi.Count <= 0)
                    r_json = Json(new { result = "Data tidak ada" }, JsonRequestBehavior.AllowGet);
                else
                {
                    r_json = Json(new { result = "", tbl_hobi }, JsonRequestBehavior.AllowGet);
                    r_json.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception e)
            {
                r_json = Json(new { result = e.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return r_json;
        }

        [HttpPost]
        public ActionResult GetDataAllHobi()
        {
            JsonResult r_json = null;
            try
            {
                query = $"SELECT 0 seq, id hobi_id, nama_hobi from tb_hobi Order By hobi_id desc";
                var tbl_hobi = ClassFunction.toObject(new ClassConnection().GetDataTable(query,"tb_hobi"));

                if(tbl_hobi == null || tbl_hobi.Count <= 0)
                {
                    r_json = Json(new { result = "Data Tidak Ada" }, JsonRequestBehavior.AllowGet);
                }else
                {
                    r_json = Json(new { result = "", tbl_hobi }, JsonRequestBehavior.AllowGet);
                    r_json.MaxJsonLength = Int32.MaxValue;
                }
            }
            catch (Exception err)
            {
                r_json = Json(new { result = err.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return r_json;
        }

        // Report Data user
        public ActionResult Report()
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                return View();
            }
            return RedirectToAction("LoginUser", "Home");
        }

        [HttpPost]
        public ActionResult ShowReport(string reportType)
        {
            var rptFile = "rptUser";
            var rptname = "ReportDataUser";
            rptFile = rptFile.Replace("PDF", "");

            var sWhere = " WHERE 1=1";

            var irequest = new ReportRequest()
            {
                rptQuery = "",
                rptFile = rptFile,
                rptPaperSizeEnum = 14,
                rptPaperOrientationEnum = 2,
                rptExportType = reportType,
                rptParam = new Dictionary<string, string>()
            };

            irequest.rptParam.Add("sWhere", sWhere);
            var rpt_id = GenerateReport(irequest);
            return Json(new { rptname, rpt_id }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult PrintReport(string ids, string type)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                string rptname = "DataUser";
                string filerptname = "User";
                ReportDocument report = new ReportDocument();
                report.Load(Path.Combine(Server.MapPath("~/Report"), "rpt" + filerptname + ".rpt"));
                report.SetParameterValue("sWhere", " WHERE uh.user_id IN (" + ids + ")");
                ClassProcedure.SetDBLogonForReport(report);
                Stream stream = report.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat);
                stream.Seek(0, SeekOrigin.Begin);
                report.Close();
                report.Dispose();
                Response.AppendHeader("content-disposition", "inline; filename=" + rptname + ".pdf");
                return new FileStreamResult(stream, "application/pdf");
            }
            return RedirectToAction("LoginUser", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
