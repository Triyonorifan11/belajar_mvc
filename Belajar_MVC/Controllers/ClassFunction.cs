﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Belajar_MVC.Models;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Security.AccessControl;
using CrystalDecisions.Shared;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;

namespace Belajar_MVC.Controllers
{
    public class table_usage
    {
        public string table_name { get; set; }
        public string column_name { get; set; }
    }

    public static class StringExtensions
    {
        public static string tChar(this string str)
        {
            return (str ?? "").Replace("'", "''");
        }

        public static bool cekDataUsage(this db_belajarEntities2 db, int id, List<string> column_name, List<string> exc_table_name)
        {
            return db.cekDataUsage(id.ToString(), column_name, exc_table_name);
        }
        public static bool cekDataUsage(this db_belajarEntities2 db, string id, List<string> column_name, List<string> exc_table_name)
        {
            var table_name = ""; var col_name = "";
            if (column_name != null)
                col_name = string.Join("','", column_name);
            if (exc_table_name != null)
                table_name = string.Join("','", exc_table_name);

            var tbl_usage = db.Database.SqlQuery<table_usage>($"SELECT ta.name table_name, col.name column_name FROM sys.tables ta INNER JOIN sys.columns col ON col.object_id=ta.object_id WHERE col.name IN ('{col_name}') AND ta.name NOT IN ('{table_name}')").ToList();

            if (tbl_usage != null && tbl_usage.Count() > 0)
            {
                foreach (var item in tbl_usage)
                {
                    if (db.Database.SqlQuery<int>($"SELECT COUNT(*) FROM {item.table_name} WHERE {item.column_name}='{id}'").FirstOrDefault() > 0)
                        return true;
                }
            }
            return false;
        }
    }
        public class ClassFunction
    {
        // GET: ClassFunction
        public static List<Dictionary<string, object>> toObject(DataTable tbl)
        {
            List<Dictionary<string, object>> rowsdtl = new List<Dictionary<string, object>>();
            if (tbl.Rows.Count > 0)
            {
                foreach (DataRow dr in tbl.Rows)
                {
                    var row = new Dictionary<string, object>();
                    foreach (DataColumn col in tbl.Columns)
                    {
                        row.Add(col.ColumnName, (dr[col] == DBNull.Value) ? "" : dr[col]);
                    }
                    rowsdtl.Add(row);
                }
            }
            return rowsdtl;
        }

        public static DateTime GetServerTime()
        {
            db_belajarEntities2 db = new db_belajarEntities2();
            return db.Database.SqlQuery<DateTime>("SELECT GETDATE() AS tanggal").FirstOrDefault();
        }

        public static IList<Dictionary<string, object>> getListDataTable(DataTableAjaxPostModel model, out int filteredResultsCount, out int totalResultsCount, string sql_select, string sql_from, string fixed_filter = "", string default_order = "")
        {
            db_belajarEntities2 db = new db_belajarEntities2();
            var take = model.length;
            var skip = model.start;

            var and_filter = "";
            if ((model.search != null) ? !string.IsNullOrEmpty(model.search.value) : false)//search in all column
            {
                var col_tosearch = model.columns.Where(w => w.searchable).ToList();
                foreach (var s in model.search.value.Trim().Split(' '))
                {
                    var or_filter = "";
                    foreach (var obj in col_tosearch)
                    {
                        if (or_filter != "") or_filter += " OR ";
                        or_filter += (string.IsNullOrEmpty(obj.alias) ? obj.data : obj.alias) + " LIKE '%" + s.tChar() + "%'";
                    }
                    if (or_filter != "")
                    {
                        and_filter += " AND (" + or_filter + ")";
                    }
                }
            }
            foreach (var obj in model.columns.Where(w => /*w.searchable &*/ w.search != null).ToList())//search by column
            {
                if (!string.IsNullOrEmpty(obj.search.value))
                {
                    if (obj.search.value.ToLower().StartsWith("between") | obj.search.value.ToLower().StartsWith("in(") | obj.search.value.StartsWith("=") | obj.search.value.StartsWith("<>"))
                    {
                        and_filter += " AND " + (string.IsNullOrEmpty(obj.alias) ? obj.data : obj.alias) + " " + obj.search.value;
                    }
                    else
                    {
                        var n_filter = "";
                        foreach (var s in obj.search.value.Split(' '))
                        {
                            if (n_filter != "") n_filter += " OR ";
                            n_filter += (string.IsNullOrEmpty(obj.alias) ? obj.data : obj.alias) + " LIKE '" + obj.search.value.tChar() + "'";
                        }
                        if (n_filter != "")
                        {
                            and_filter += " AND (" + n_filter + ")";
                        }
                    }
                }
            }

            string orderBy = "";
            if (model.order != null)
            {
                foreach (var o in model.order)
                {
                    if (!string.IsNullOrEmpty(orderBy)) orderBy += ", ";
                    orderBy += model.columns[o.column].data + " " + o.dir.ToLower();
                }
            }

            // search the dbase taking into consideration table sorting and paging
            if (string.IsNullOrEmpty(orderBy))
            {
                if (string.IsNullOrEmpty(default_order))
                    default_order = model.columns.Select(s => s.data).First() + " DESC";
                orderBy = default_order;
            }

            var result = ClassFunction.toObject(new ClassConnection().GetDataTable("SELECT " + sql_select + " from " + sql_from + " WHERE 1=1 " + and_filter + fixed_filter + " ORDER BY " + orderBy + " OFFSET " + skip + " ROWS FETCH NEXT " + take + " ROWS ONLY", "result"));

            // now just get the count of items (without the skip and take) - eg how many could be returned with filtering
            filteredResultsCount = db.Database.SqlQuery<int>("Select count(1) from " + sql_from + " WHERE 1=1 " + and_filter + fixed_filter + " ").DefaultIfEmpty(0).First();
            totalResultsCount = db.Database.SqlQuery<int>("Select count(1) from " + sql_from + " WHERE 1=1 " + fixed_filter).DefaultIfEmpty(0).First();
            if (result == null)
            {
                // empty collection...
                return new List<Dictionary<string, object>>();
            }
            return result;
        }
        public static object MappingTable(object tbl_dest, object tbl_source)
        {
            foreach (var prop in tbl_dest.GetType().GetProperties())
            {
                var prop_source = tbl_source.GetType().GetProperties().Where(g => g.Name == prop.Name).FirstOrDefault();
                if (prop_source != null)
                {
                    var fieldvalue = prop_source.GetValue(tbl_source, null);
                    prop.SetValue(tbl_dest, fieldvalue);
                }
            }
            return tbl_dest;
        }

        public static string getErrorMessage(System.Data.Entity.Validation.DbEntityValidationException e)
        {
            var err = "";
            foreach (var eve in e.EntityValidationErrors)
            {
                err += "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:<br />";
                foreach (var ve in eve.ValidationErrors)
                {
                    err += "- Property: " + ve.PropertyName + ", Error: " + ve.ErrorMessage + "<br />";
                }
            }
            return err;
        }

    }

    public class ClassProcedure
    {
        //public static string SetDBLogonForReport(ReportDocument reportDoc)
        //{
        //    ConnectionInfo connectionInfo = new ConnectionInfo();
        //    connectionInfo.ServerName= System.Configuration.ConfigurationManager.AppSettings["DB-Server"];
        //    connectionInfo.DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DB-Name"];
        //    //connectionInfo.UserID = System.Configuration.ConfigurationManager.AppSettings["DB-User"];
        //    //connectionInfo.Password = System.Configuration.ConfigurationManager.AppSettings["DB-Password"];
        //    connectionInfo.IntegratedSecurity = true;

        //    Tables tables = reportDoc.Database.Tables;
        //    foreach (Table table in tables)
        //    {
        //        TableLogOnInfo tableLogonInfo = table.LogOnInfo;
        //        tableLogonInfo.ConnectionInfo = connectionInfo;
        //        table.ApplyLogOnInfo(tableLogonInfo);
        //    }

        //    foreach (ReportDocument subreport in reportDoc.Subreports)
        //    {
        //        Tables myTablesSub = subreport.Database.Tables;
        //        foreach (Table table in myTablesSub)
        //        {
        //            TableLogOnInfo tableLogonInfo = table.LogOnInfo;
        //            tableLogonInfo.ConnectionInfo = connectionInfo;
        //            table.ApplyLogOnInfo(tableLogonInfo);
        //        }
        //    }

        //    return "OK";
        //}
        private static ConnectionInfo getConnectionInfo()
        {
            ConnectionInfo connectionInfo = new ConnectionInfo();
            var conStr = System.Configuration.ConfigurationManager.ConnectionStrings["db_belajarEntities"].ConnectionString;
            var ecsb = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder(conStr);
            var csb = new SqlConnectionStringBuilder(ecsb.ProviderConnectionString);
            connectionInfo.IntegratedSecurity = csb.IntegratedSecurity;
            connectionInfo.ServerName = csb.DataSource;
            connectionInfo.DatabaseName = csb.InitialCatalog;
            connectionInfo.UserID = csb.UserID;
            connectionInfo.Password = csb.Password;
            return connectionInfo;
        }

        public static string SetDBLogonForReport(ReportDocument reportDoc)
        {
            ConnectionInfo connectionInfo = getConnectionInfo();

            Tables tables = reportDoc.Database.Tables;
            foreach (Table table in tables)
            {
                TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                tableLogonInfo.ConnectionInfo = connectionInfo;
                table.ApplyLogOnInfo(tableLogonInfo);
            }

            foreach (ReportDocument subreport in reportDoc.Subreports)
            {
                Tables myTablesSub = subreport.Database.Tables;
                foreach (Table table in myTablesSub)
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = connectionInfo;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }
            }

            return "OK";
        }


        public static string SetDBLogonForReport(ReportDocument reportDoc, string[] exctablename)
        {
            ConnectionInfo connectionInfo = getConnectionInfo();

            Tables tables = reportDoc.Database.Tables;
            foreach (Table table in tables)
            {
                if (!exctablename.Contains(table.Name))
                {
                    TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                    tableLogonInfo.ConnectionInfo = connectionInfo;
                    table.ApplyLogOnInfo(tableLogonInfo);
                }
            }

            foreach (ReportDocument subreport in reportDoc.Subreports)
            {
                Tables myTablesSub = subreport.Database.Tables;
                foreach (Table table in myTablesSub)
                {
                    if (!exctablename.Contains(table.Name))
                    {
                        TableLogOnInfo tableLogonInfo = table.LogOnInfo;
                        tableLogonInfo.ConnectionInfo = connectionInfo;
                        table.ApplyLogOnInfo(tableLogonInfo);
                    }
                }
            }

            return "OK";
        }

        public static string SetDBLogonForReportTable(Table reportTbl)
        {
            ConnectionInfo connectionInfo = getConnectionInfo();

            TableLogOnInfo tableLogonInfo = reportTbl.LogOnInfo;
            tableLogonInfo.ConnectionInfo = connectionInfo;
            reportTbl.ApplyLogOnInfo(tableLogonInfo);

            return "OK";
        }
    }
}