﻿using Belajar_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Belajar_MVC.Controllers
{
    public class HobiController : Controller
    {
        private db_belajarEntities2 db = new db_belajarEntities2();
        // GET: Hobi
        public ActionResult Index()
        {
            m_tb_hobi tbl;
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                tbl = new m_tb_hobi();
                tbl.create_by = Session["nama_user"].ToString();
                tbl.create_at = ClassFunction.GetServerTime();
                tbl.update_at = ClassFunction.GetServerTime();
                tbl.update_by = Session["nama_user"].ToString();
                return View(tbl);
            }
            return RedirectToAction("LoginUser", "Home");
        }

        // GET: Hobi/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Hobi/Create
        public ActionResult Create()
        {
            return RedirectToAction("index");
        }

        // POST: Hobi/Create
        [HttpPost]
        public ActionResult Create(m_tb_hobi model)
        {
            if(Session["email_user"] != null || Session["nama_user"] != null)
            {
                try
                {
                    // TODO: Add insert logic here
                    if (ModelState.IsValid)
                    {
                        var check_Nama_Hobi = db.tb_hobi.Where(data => data.nama_hobi.Contains(model.nama_hobi)).FirstOrDefault();
                        if(check_Nama_Hobi != null)
                        {
                            ModelState.AddModelError("nama_hobi", "Hobi Sudah Tersedia");
                        }else
                        {
                            var data_tbl_hobi = new tb_hobi()
                            {
                                nama_hobi = HttpUtility.HtmlDecode(model.nama_hobi),
                                create_by = HttpUtility.HtmlDecode(model.create_by),
                                create_at = model.create_at,
                                update_by = HttpUtility.HtmlDecode(model.update_by),
                                update_at = model.update_at
                            };
                            db.tb_hobi.Add(data_tbl_hobi);
                            db.SaveChanges();
                            Session["success"] = "Berhasil Tambah Hobi";
                            return RedirectToAction("Index", "Hobi");
                        }
                    }
                    foreach (var error in ViewData.ModelState.Values.SelectMany(v => v.Errors)){
                        Session["error"] = error.ErrorMessage;
                    };
                    return RedirectToAction("Index", "Hobi");
                }
                catch(Exception ex)
                {
                    Session["error"] = ex;
                    return View("index", ex);
                }
            }
            return RedirectToAction("LoginUser", "Home");
        }

        // GET: Hobi/Edit/5
        public ActionResult Edit(int id)
        {
            return RedirectToAction("index", "Hobi");
        }

        // POST: Hobi/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("index", "Hobi");
            }
            catch
            {
                return View();
            }
        }

        // GET: Hobi/Delete/5
        public ActionResult Delete(int id)
        {
            return RedirectToAction("index", "Hobi");
        }

        // POST: Hobi/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, tb_hobi model)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {
                try
                {
                    // TODO: Add delete logic here
                    var tbl_hobi = db.tb_hobi.Where(data => data.id == id).FirstOrDefault();
                    if(tbl_hobi != null)
                    {
                        db.tb_hobi.Remove(tbl_hobi);
                        db.SaveChanges();
                        Session["success"] = "Berhasil Hapus Data";
                        return RedirectToAction("Index", "Hobi");
                    }
                    foreach (var error in ViewData.ModelState.Values.SelectMany(v => v.Errors))
                    {
                        Session["error"] = error.ErrorMessage;
                    };
                    return RedirectToAction("Index", "Hobi");
                }
                catch(Exception err)
                {
                    Session["error"] = err;
                    return RedirectToAction("Index", "Hobi");
                }
            }
            return RedirectToAction("LoginUser", "Home");
        }


        public JsonResult getListDataHobi(DataTableAjaxPostModel model)
        {
            if (Session["email_user"] != null || Session["nama_user"] != null)
            {

                var sql_select = "*";
                var sql_from = "tb_hobi";
                var sFixedFilter = "";
                var sOrder_by = "id desc";
                int filteredResultsCount;
                int totalResultsCount;
                var result = ClassFunction.getListDataTable(model, out filteredResultsCount, out totalResultsCount, sql_select, sql_from, sFixedFilter, sOrder_by);
                return Json(
                    new
                    {
                        draw = model.draw,
                        recordsTotal = totalResultsCount,
                        recordsFiltered = filteredResultsCount,
                        data = result
                    });
            }
            return Json(new
            {
                draw = 0,
                recordsTotal = 0,
                recordsFiltered = 0,
                data = 0,
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
