﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Belajar_MVC.Models;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Security.AccessControl;

namespace Belajar_MVC.Controllers
{
    public class HomeController : Controller
    {
        private db_belajarEntities2 db = new db_belajarEntities2();
        public ActionResult Index()
        {
            int jam = DateTime.Now.Hour;
            if (jam > 6 & jam < 12)
            {
                ViewBag.sapa = "Selamat Pagi";
            } else if (jam > 12 & jam < 18)
            {
                ViewBag.sapa = "Selamat Siang";
            } else
            {
                ViewBag.sapa = "Selamat malam";
            }
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult index(FormRegisterModel model)
        {
            int jam = DateTime.Now.Hour;
            if (jam > 6 & jam < 12)
            {
                ViewBag.sapa = "Selamat Pagi";
            }
            else if (jam > 12 & jam < 18)
            {
                ViewBag.sapa = "Selamat Siang";
            }
            else
            {
                ViewBag.sapa = "Selamat malam";
            }

            if (ModelState.IsValid)
            {
                var tb_user = new tb_user()
                {
                    Email = model.Email,
                    Password = model.Password
                };
                db.tb_user.Add(tb_user);
                db.SaveChanges();
                ViewBag.confirm = "Berhasil register";
                return View("Index");
            }

            return View(model);
        }

        public ActionResult DaftarUser()
        {
            var data_user = db.tb_user.ToList();
            return View(data_user);
        }

        public ActionResult LoginUser()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LoginUser(LoginUserModel model)
        {
            if (ModelState.IsValid)
            {
                var get_data_user = db.tb_user.Where(data => data.Email.Equals(model.Email)).FirstOrDefault();
                if (get_data_user != null)
                {
                    if (model.Password.Equals(get_data_user.Password))
                    {
                        SetUserSession(get_data_user.Email);
                        return RedirectToAction("index", "Dashboard");
                    }
                    else
                    {
                        ModelState.AddModelError("Password", "Password atau email salah");
                    }
                }
                else
                {
                    ModelState.AddModelError("Email", "Password atau email salah");
                }
            }
            return View(model);
        }

        private void SetUserSession(string email)
        {
            var tbl = db.tb_user.Where(data => data.Email.ToLower().Equals(email)).FirstOrDefault();
            Session["id_user"] = tbl.Id;
            Session["email_user"] = tbl.Email;
            Session["nama_user"] = tbl.Nama;
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("index", "Dashboard");
        }


        public ActionResult About()
        {

            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}