﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Web;

namespace Belajar_MVC
{
    public class GlobalExtentions : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the Web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            //write your handler implementation here.
        }

        #endregion
    }
        public static class StringExt
        {
            public const string FileExt_Report = ".exported-rpt";

            public static string AsTempPath(this string filenameNoExt, string ext = "")
            {
                string temp = HttpContext.Current.Server.MapPath("~/UploadedFiles/FileTemps");
                if (!Directory.Exists(temp))
                {
                    DirectorySecurity securityRules = new DirectorySecurity();
                    securityRules.AddAccessRule(new FileSystemAccessRule("Everyone", FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
                    Directory.CreateDirectory(temp, securityRules);
                }
                temp = Path.Combine(temp, filenameNoExt + ext);
                return temp;
                //return System.IO.Path.GetTempPath() + filenameNoExt + ext;
            }
        }
}
