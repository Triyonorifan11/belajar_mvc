﻿using Belajar_MVC.Controllers;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.IO;
using System.Web;
using static Belajar_MVC.GlobalClass;

namespace Belajar_MVC
{
    public class GlobalFunction : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the Web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            //write your handler implementation here.
        }

        #endregion
        public static string GenerateReport(ReportRequest request)
        {
            var report = new ReportDocument();
            report.Load(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~"), "Report/" + request.rptFile + ".rpt"));
            if (string.IsNullOrEmpty(request.rptQuery) && request.rptDataSource != null)
                report.SetDataSource(request.rptDataSource);
            else if (!string.IsNullOrEmpty(request.rptQuery))
            {
                DataTable dtRpt = new ClassConnection().GetDataTable(request.rptQuery, request.rptFile);
                report.SetDataSource(dtRpt);
            }
            if (request.rptParam != null && request.rptParam.Count > 0)
                foreach (var item in request.rptParam)
                    report.SetParameterValue(item.Key, item.Value);
            if (string.IsNullOrEmpty(request.rptQuery) && request.rptDataSource == null)
                ClassProcedure.SetDBLogonForReport(report);
            if (request.rptPaperSizeEnum != 0)
                report.PrintOptions.PaperSize = (CrystalDecisions.Shared.PaperSize)request.rptPaperSizeEnum;
            if (request.rptPaperOrientationEnum != 0)
                report.PrintOptions.PaperOrientation = (CrystalDecisions.Shared.PaperOrientation)request.rptPaperOrientationEnum;
            var reportId = Guid.NewGuid();
            var saveAs = reportId.ToString().AsTempPath(StringExt.FileExt_Report);
            report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.CrystalReport, saveAs);
            report.Dispose(); report.Close();

            var sdir = System.IO.Path.GetTempPath();
            if (Directory.Exists(sdir))
            {
                foreach (var file in Directory.GetFiles(sdir))
                {
                    var updtime = File.GetLastWriteTime(file);
                    if (updtime < DateTime.Parse(DateTime.Now.ToString("dd/MM/yyyy 00:00:00")) && file.EndsWith(".exported-rpt"))
                    {
                        File.Delete(file);
                    }
                }
            }

            return reportId.ToString();
        }
    }
}
