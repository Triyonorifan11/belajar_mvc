﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewerNew.aspx.cs" Inherits="Belajar_MVC.Other.ReportViewerNew" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        html, body, form { width: 100%; height: 100%; margin: 0; padding: 0 }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <CR:CrystalReportViewer ID="CRViewer" runat="server"  Width="100%" Height="100%" AutoDataBind="True" HasCrystalLogo="False" HasDrillUpButton="False" HasExportButton="False" HasPrintButton="False" ToolPanelView="None" HasDrilldownTabs="False" EnableDrillDown="False" HasToggleParameterPanelButton="False" />
            <button type="button" style="" onclick="getElementById('IconImg_CRViewer_toptoolbar_prevPg').click()">Prev</button>
            <button type="button" style="" onclick="getElementById('IconImg_CRViewer_toptoolbar_nextPg').click()">Next</button>
        </div>
    </form>
</body>
</html>
