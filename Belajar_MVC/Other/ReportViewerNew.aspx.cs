﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;


namespace Belajar_MVC.Other
{
    public partial class ReportViewerNew : System.Web.UI.Page
    {
        ReportDocument doc = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            string reportId = Request.QueryString["report"];
            string rpttype = Request.QueryString["rpttype"];
            string rptname = Request.QueryString["rptname"];
            if (reportId != null)
            {
                string filePath = reportId.ToString().AsTempPath(StringExt.FileExt_Report);
                if (File.Exists(filePath))
                {
                    doc.Load(filePath);
                    if (rpttype == "PDF")
                    {
                        Response.Buffer = false; Response.ClearContent(); Response.ClearHeaders();
                        doc.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, true, rptname);
                        doc.Close(); doc.Dispose();
                    }
                    else if (rpttype == "XLS")
                    {
                        Response.Buffer = false; Response.ClearContent(); Response.ClearHeaders();
                        doc.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.Excel, Response, true, rptname);
                        doc.Close(); doc.Dispose();
                    }
                    else if (rpttype == "CSV")
                    {
                        Response.Buffer = false; Response.ClearContent(); Response.ClearHeaders();
                        doc.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.Excel, Response, true, rptname);
                        doc.Close(); doc.Dispose();
                    }
                }
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            CRViewer.ReportSource = doc;
        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            doc.Dispose();
        }
    }
}